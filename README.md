"Legislators in the Crossfire: Strategic Non-Voting and the Effect of Transparency", European Journal of Political Economy, Héloïse Cloléry (2022)

Replication Folder Details

4 scripts are written in Stata:
	
	- "00_Master.do" runs all Stata scripts
	
	- "11_Estimations.do" provides the regression results presented in the paper
	
	- "12_WNominate.do" is necessary to run the R script and create the Wnominate map on MPs' positions
	
	- "13_MapPresence.do" creates the map showing the different constituencies


1 script is written in R:
	
	- "WN.R" creates the Wnominate map, must be launched after the "00_Master.do" file


The folder contains 6 datasets
	
	- "Rollcall.dta" is the main dataset containing all the recorded votes. Data dictionnary and details on how the data was constructed can be found on "Rollcall data.pdf"
	
	- "salienceCitizens.dta" has been downloaded from https://www.insee.fr/fr/statistiques/2383052#tableau-figure1. Each variable represent the percentage of the population above 14 years old that considered the topic as the most important one for the different years.
	
	- "results depart 2015.dta" is the electoral results from the 2015 departmental elections and was downloaded from https://www.data.gouv.fr/fr/datasets/elections-departementales-2015-resultats-tour-1/
	
	- "presidential_results1stRound_2012.dta" is the electoral results from the 2012 presidential election and was downloaded from https://www.data.gouv.fr/fr/datasets/election-presidentielle-2012-resultats-572124/
	
	- "map_shp.dta" is the geographical data to represent graphically the different constituencies. The data was downloaded from https://www.data.gouv.fr/fr/datasets/carte-des-circonscriptions-legislatives-2012-et-2017/
	
	- "IdF_shp.dta" is the same geographical data, but for the region of Paris only.